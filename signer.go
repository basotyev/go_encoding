package middterm

import (
	"strconv"
	"sync"
	"time"
)

// сюда писать код

const quota = 1

func StartJob(job job, in, out chan interface{}, group *sync.WaitGroup) {
	defer group.Done()
	job(in, out)
	time.Sleep(800*time.Millisecond)
}

func ExecutePipeline(jobs ...job) {
	wg := &sync.WaitGroup{}
	// The first job is for input data and the last for the output
	// So, we take the length of the given sequence and in the loop below
	// check out whether a job is first or last
	for i, j := range jobs{
		wg.Add(1)
		in := make(chan interface{}, 100)
		mid := make(chan interface{}, 100)
		out := make(chan interface{}, 100)
		switch i {
			case 0:
				go StartJob(j, in, in, wg)
			case 1:
				go StartJob(j, in, mid, wg)
			case 2:
				go StartJob(j, mid, out, wg)
			case 3:
				go StartJob(j, out, out, wg)
		}
	}
	wg.Wait()
}

func SingleHash(in chan interface{}, out chan interface{}) {
	wg := &sync.WaitGroup{}
	quoteCh := make(chan struct{}, quota)

	crc32Ch := make(chan interface{}, 1)
	md5Ch := make(chan interface{}, 1)
	var crc32Results, md5Results []string

	data := <- in
	wg.Add(1)
	go func(data string, out chan interface{}, wg *sync.WaitGroup) {
		out <- DataSignerCrc32(data) + "~"
		wg.Done()
	}(data.(string), crc32Ch, wg)
	wg.Add(1)
	go func(quote chan struct{}, data string, out chan interface{}, wg *sync.WaitGroup) {
		quote <- struct{}{}
		out <- DataSignerCrc32(DataSignerMd5(data))
		wg.Done()
		<- quote
	}(quoteCh, data.(string), md5Ch, wg)

	counter := 0
	var result string
	for crc := range crc32Ch {
		crc32Results = append(crc32Results, (crc).(string))
		result += crc32Results[counter] + md5Results[counter]
	}
	for md := range md5Ch {
		md5Results = append(md5Results, (md).(string))
	}
	wg.Wait()
	//close(quoteCh)
	//close(crc32Ch)
	//close(md5Ch)
	out <- result
}

func MultiHash(in chan interface{}, out chan interface{}) {
	data := <-in
	go func(in string) {
		var res string
		var tmp [5]string
		wg := &sync.WaitGroup{}
		innerChannel := make(chan []string, 1)
		for i:= 0; i < 5; i++ {
			wg.Add(1)
			go func(out chan []string, group *sync.WaitGroup) {
				var tmp []string
				tmp = append(tmp, strconv.Itoa(i))
				tmp = append(tmp, DataSignerCrc32(data.(string)))
				innerChannel <- tmp
				group.Done()
			}(innerChannel, wg)
		}
		for j := range innerChannel {
			index, _ := strconv.Atoi(j[0])
			tmp[index] = j[1]
		}
		wg.Wait()
		close(innerChannel)
		for _, word := range tmp {
			res += word
		}
		out <- res
	}(data.(string))
}

func CombineResults(in chan interface{}, out chan interface{}) {
	var output string
	for i := range in {
		output += i.(string)
	}
	time.Sleep(500 * time.Millisecond)
	out <- output
}
